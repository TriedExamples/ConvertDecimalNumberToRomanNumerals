//
//  Int+Converter.swift
//  ConvertDecimalNumberToRomanNumerals
//
//  Created by vishnusankar on 14/06/18.
//  Copyright © 2018 vishnusankar. All rights reserved.
//

import Foundation

extension Int {
    
    //Time Complexity log10(0) + log(log10(0))
    public func romanNumerical() -> String {
        var romanNumerical = ""
        let numberOfDigit = self.numberOfDigits()
        
        for i in (1...numberOfDigit).reversed() {
            let currentPlaceValues = Int(pow(10, Double((numberOfDigit-1)-(i-1))))
            print(self.array[i-1])
            let currentPlacedigitValue = self.array[i-1] * currentPlaceValues
            
            if currentPlacedigitValue < 10 {
                romanNumerical = "\(self.singleDigitNumberToRoman(value: currentPlacedigitValue))\(romanNumerical)"
            }
            else if currentPlacedigitValue >= 10 && currentPlacedigitValue < 100 {
                romanNumerical = "\(self.doubleDigitNumberToRoman(value: currentPlacedigitValue))\(romanNumerical)"
            }
            else if currentPlacedigitValue >= 100 && currentPlacedigitValue < 1000 {
                romanNumerical = "\(self.threeDigitNumberToRoman(value: currentPlacedigitValue))\(romanNumerical)"
            }
            else if currentPlacedigitValue >= 1000 && currentPlacedigitValue < 4000 {
                romanNumerical = "\(self.fourDigitNumberToRoman(value: currentPlacedigitValue))\(romanNumerical)"
            }
        }
        return romanNumerical
    }
    
    //Time Complexity log(1)
    func singleDigitNumberToRoman(value : Int) -> String {
    
        var romanCharacters : String = String()
        
        switch value {
        case 1:
            romanCharacters = "I"
        case 2:
            romanCharacters = "II"
        case 3:
            romanCharacters = "III"
        case 4:
            romanCharacters = "IV"
        case 5:
            romanCharacters = "V"
        case 6:
            romanCharacters = "VI"
        case 7:
            romanCharacters = "VII"
        case 8:
            romanCharacters = "VIII"
        case 9:
            romanCharacters = "IX"
        default:
            romanCharacters = ""
        }
        return romanCharacters
    }
    
    //Time Complexity log(1)
    func doubleDigitNumberToRoman(value : Int) -> String {
        
        var romanCharacters : String = String()
        
        switch value {
        case 10:
            romanCharacters.append("X")
        case 20:
            romanCharacters.append("XX")
        case 30:
            romanCharacters.append("XXX")
        case 40:
            romanCharacters.append("XL")
        case 50:
            romanCharacters.append("L")
        case 60:
            romanCharacters.append("LX")
        case 70:
            romanCharacters.append("LXX")
        case 80:
            romanCharacters.append("LXXX")
        case 90:
            romanCharacters.append("XC")
        default:
            romanCharacters.append("")
        }
        
        return romanCharacters
    }

    //Time Complexity log(1)
    func threeDigitNumberToRoman(value : Int) -> String {
        
        var romanCharacters : String = String()
        
        switch value {
        case 100:
            romanCharacters.append("C")
        case 200:
            romanCharacters.append("CC")
        case 300:
            romanCharacters.append("CCC")
        case 400:
            romanCharacters.append("CD")
        case 500:
            romanCharacters.append("D")
        case 600:
            romanCharacters.append("DC")
        case 700:
            romanCharacters.append("DCC")
        case 800:
            romanCharacters.append("DCCC")
        case 900:
            romanCharacters.append("CM")
        default:
            romanCharacters.append("")
        }
        
        return romanCharacters
    }
    
    //Time Complexity log(1)
    func fourDigitNumberToRoman(value : Int) -> String {
        
        var romanCharacters : String = String()
        
        switch value {
        case 1000:
            romanCharacters.append("M")
        case 2000:
            romanCharacters.append("MM")
        case 3000:
            romanCharacters.append("MMM")
        default:
            romanCharacters.append("")
        }
        
        return romanCharacters
    }

    public func numberOfDigits() -> Int {
        return self.numberOfDigits(value: self)
    }
    
    //Time Complexity log10(0)
    private func numberOfDigits(value : Int) -> Int {
        if value < 10 {
            return 1
        }else {
            return 1 + numberOfDigits(value: value/10)
        }
    }
    
    var array : [Int] {
        return String(self).compactMap{ Int(String($0)) }
    }
}
